package main

import (
	. "../../helpers"
	"strconv"
	"strings"
)

func main() {
	lines := ReadFile("./2023/2/input.txt")

	part1(lines)
}

const MAX_RED = 12
const MAX_BLUE = 14
const MAX_GREEN = 13

type Game struct {
	gameId   int
	maxRed   int
	maxGreen int
	maxBlue  int
	power    int
}

func part1(lines []string) {
	var allGames []Game
	var possibleGames []Game

	for _, line := range lines {
		game := parseGame(line)

		if game.maxRed <= MAX_RED && game.maxBlue <= MAX_BLUE && game.maxGreen <= MAX_GREEN {
			possibleGames = append(possibleGames, game)
		}

		allGames = append(allGames, game)
	}

	result := 0
	totalPower := 0
	for _, game := range possibleGames {
		result += game.gameId
	}
	for _, game := range allGames {
		totalPower += game.power
	}

	println(result)
	println(totalPower)
}

const GAMEID_SEPERATOR = ": "

func parseGame(s string) Game {
	split := strings.Split(s, GAMEID_SEPERATOR)
	idString, gameString := split[0], split[1]

	gameIdString := strings.ReplaceAll(idString, "Game ", "")
	gameId, _ := strconv.Atoi(gameIdString)

	reveals := strings.Split(gameString, "; ")

	game := Game{
		gameId:   gameId,
		maxRed:   0,
		maxGreen: 0,
		maxBlue:  0,
	}

	for _, reveal := range reveals {
		colors := strings.Split(reveal, ", ")

		for _, color := range colors {
			cS := strings.Split(color, " ")
			number, _ := strconv.Atoi(cS[0])

			if cS[1] == "red" && number > game.maxRed {
				game.maxRed = number
			}
			if cS[1] == "blue" && number > game.maxBlue {
				game.maxBlue = number
			}
			if cS[1] == "green" && number > game.maxGreen {
				game.maxGreen = number
			}
		}

	}

	game.power = game.maxRed * game.maxBlue * game.maxGreen

	return game
}
