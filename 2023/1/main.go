package main

import (
	. "../../helpers"
	"sort"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	lines := ReadFile("./2023/1/input.txt")
	part1(lines)
	part2(lines)
}

//Between 54711 and 54738

func part1(lines []string) {
	total := 0
	for _, line := range lines {
		var digits []rune
		for _, char := range line {
			if unicode.IsDigit(char) {
				digits = append(digits, char)
			}
		}

		if len(digits) > 1 {
			digit := string(digits[0]) + string(digits[len(digits)-1])
			temp, _ := strconv.Atoi(digit)
			total += temp
		} else if len(digits) == 1 {
			digit := string(digits[0]) + string(digits[0])
			temp, _ := strconv.Atoi(digit)
			total += temp
		}

	}

	println(total)
}

type Numberstart struct {
	start  int
	number string
}

func part2(lines []string) {
	total := 0

	for _, line := range lines {
		var splitLine []string
		lastSplit := 0
		for i, char := range line {
			if unicode.IsDigit(char) {
				if i > 0 {
					splitLine = append(splitLine, line[lastSplit:i])
				}
				splitLine = append(splitLine, string(char))
				lastSplit = i + 1
			}
		}
		if len(line)-1 > lastSplit {
			splitLine = append(splitLine, line[lastSplit:len(line)])
		}

		var digits []string
		for _, part := range splitLine {
			var numbers []Numberstart

			numbers = containsDigit(part, "one", "1", numbers)
			numbers = containsDigit(part, "two", "2", numbers)
			numbers = containsDigit(part, "three", "3", numbers)
			numbers = containsDigit(part, "four", "4", numbers)
			numbers = containsDigit(part, "five", "5", numbers)
			numbers = containsDigit(part, "six", "6", numbers)
			numbers = containsDigit(part, "seven", "7", numbers)
			numbers = containsDigit(part, "eight", "8", numbers)
			numbers = containsDigit(part, "nine", "9", numbers)

			if len(part) == 1 {
				runeArr := []rune(part)
				if unicode.IsDigit(runeArr[0]) {
					digits = append(digits, part)
				}
			}

			sort.Slice(numbers, func(i, j int) bool {
				return numbers[i].start < numbers[j].start
			})

			for _, number := range numbers {
				digits = append(digits, number.number)
			}

		}
		if len(digits) > 1 {
			digit := string(digits[0]) + string(digits[len(digits)-1])
			temp, _ := strconv.Atoi(digit)
			total += temp
		} else if len(digits) == 1 {
			digit := string(digits[0]) + string(digits[0])
			temp, _ := strconv.Atoi(digit)
			total += temp
		}

	}
	println(total)
}

func containsDigit(s string, substring string, numberString string, numbers []Numberstart) []Numberstart {
	count := strings.Count(s, substring)

	if count == 0 {
		return numbers
	} else if count == 1 {
		numbers = append(numbers, Numberstart{start: strings.Index(s, substring), number: numberString})
	} else {
		numbers = append(numbers, Numberstart{start: strings.Index(s, substring), number: numberString})
		numbers = append(numbers, Numberstart{start: strings.LastIndex(s, substring), number: numberString})
	}

	return numbers
}
