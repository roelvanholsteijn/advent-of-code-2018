package main

import (
	"fmt"
)

func main() {
	lines := readFile("input.txt")

	frequency := 0

	for i := 0; i < len(lines); i++ {
		in := lineToInstruction(lines[i])

		if in.instructionType == "+" {
			frequency += in.value
		}
		if in.instructionType == "-" {
			frequency -= in.value
		}
	}

	fmt.Print(frequency)
}

