package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type instruction struct {
	instructionType string
	value int
}

func lineToInstruction(line string) instruction {
	value := line[1:]
	valueInt, _ := strconv.Atoi(value)

	return instruction{line[:1], valueInt}
}

func readFile(fileName string) []string {
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}

	lines := strings.Split(string(content), "\n")

	return lines
}

func containsInt(arr []int, value int) bool {
	for _, a := range arr {
		if a == value {
			return true
		}
	}

	return false
}