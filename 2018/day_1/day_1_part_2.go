package main

import (
	"fmt"
)

func main() {
	lines := readFile("input.txt")

	frequency := []int{0}
	counter := 0
	for  {
		for i := 0; i < len(lines); i++ {
			in := lineToInstruction(lines[i])

			if in.instructionType == "+" {
				newValue := frequency[i + (counter * len(lines))] + in.value
				contains := containsInt(frequency, newValue)
				if contains {
					fmt.Print(newValue)
					return
				}
				frequency = append(frequency, newValue)
			}
			if in.instructionType == "-" {
				newValue := frequency[i + (counter * len(lines))] - in.value

				contains := containsInt(frequency, newValue)
				if contains {
					fmt.Print(newValue)
					return
				}
				frequency = append(frequency, newValue)
			}
		}
		counter += 1
	}
}
