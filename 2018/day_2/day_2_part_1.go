package main

import (
	. "../../helpers"
	"fmt"
	"strings"
)

func main() {
	lines := ReadFile("./day_2/input.txt")

	fmt.Print(lines)

	counter2 := 0
	counter3 := 0

	for i := 0; i < len(lines); i++ {
		a := makeCountDict(lines[i])

		if a.count2 {
			counter2 += 1
		}

		if a.count3 {
			counter3 += 1
		}
	}

	fmt.Print("\n")
	fmt.Print(counter2 * counter3)
	fmt.Print("\n")
}

type count struct {
	count2 bool
	count3 bool
}

func makeCountDict(sInput string) count {
	dict := make(map[string]int)

	stringArray := strings.Split(sInput, "")

	for i := 0; i < len(stringArray); i++ {
		dict[stringArray[i]] += 1
	}

	c := count{false, false}
	for i := 0; i < len(stringArray); i++ {
		if dict[stringArray[i]] == 2{
			c.count2 = true
		}
		if dict[stringArray[i]] == 3 {
			c.count3 = true
		}
	}

	return c
}

func checkPartTwo(sInput string) {

}