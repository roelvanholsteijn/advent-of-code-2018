package main

import (
	. "../../helpers"
	"fmt"
	"os"
	"sort"
)

func main() {
	lines := ReadFile("./2020/10/input.txt")

	adapters := ConvertStringArrayToIntArray(lines)
	adapters = append(adapters, 0)
	sort.Ints(adapters)
	adapters = append(adapters, adapters[len(adapters) - 1] + 3)

	count1, count2, count3 := 0, 0, 0

	for i := 1; i < len(adapters); i++{
		switch adapters[i] - adapters[i-1] {
		case 1:
			count1++
		case 2:
			count2++
		case 3:
			count3++
		}
	}

	PrintOutput(1, count1 * count3)
	if count2 != 0 {
		fmt.Println("the current calculation for part 2 doesn't work")
		os.Exit(1)
	}
	PrintOutput(2, calculateAllPossibilities(adapters))
}

//this calculation only works if there are no adapters apart by 2 and the input parts are no larger then 5
func calculateAllPossibilities(input []int) int{
	var inputParts [][]int

	lastSlice := 0
	for i := 1; i < len(input); i ++{
		if input[i] - input[i-1] == 3 {
			inputParts = append(inputParts, input[lastSlice: i])
			lastSlice = i
		}
	}

	var productA []int
	for _, i := range inputParts {
		if len(i) < 3 {
			productA = append(productA, 1)
			continue
		}
		if len(i) == 3 {
			productA = append(productA, 2)
			continue
		}
		if len(i) == 4 {
			productA = append(productA, 4)
			continue
		}
		if len(i) == 5 {
			productA = append(productA, 7)
			continue
		}
		if len(i) > 5 {
			fmt.Println(i)
			os.Exit(1)
		}
	}

	answer := 1
	for _, i := range productA {
		answer = answer * i
	}

	return answer
}