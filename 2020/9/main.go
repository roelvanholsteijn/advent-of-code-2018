package main

import (
	. "../../helpers"
	"fmt"
	"strconv"
)

func main() {
	lines := ReadFile("./2020/9/input.txt")
	var numbers []int

	for _, line := range lines {
		num, _ := strconv.Atoi(line)
		numbers = append(numbers, num)
	}

	preamble := numbers[0:25]

	for i := 25; i < len(numbers); i++ {
		num := numbers[i]
		if checkIfNumbersHaveSum(preamble, num) {
			index := i % 25
			preamble[index] = num
		} else {
			PrintOutput(1, num)
			PrintOutput(2, getEncryptionWeaknessNumbers(numbers, num))
			break
		}
	}
}


func checkIfNumbersHaveSum(numbers []int, number int) bool {
	for a := 0; a < len(numbers); a++ {
		for b := 0; b < len(numbers); b++ {
			if a == b {
				continue
			}
			if numbers[a] + numbers[b] == number {
				fmt.Printf("number: %d == a: %d + b: %d\n", number, numbers[a], numbers[b])
				return true
			}
		}
	}
	return false
}

func getEncryptionWeaknessNumbers(numbers []int, sum int) int{
	for a := 0; a < len(numbers); a++ {
		numA := numbers[a]
		for b := a + 1; b < len(numbers); b++ {
			if numA + numbers[b] > sum {
				continue
			}

			if numA + numbers[b] == sum {
				return calculateEncryptionWeakness(numbers[a: b + 1])
			}

			numA += numbers[b]
		}
	}

	return 0
}

func calculateEncryptionWeakness(numbers []int) int{
	lowest, highest := 10000000000, 0

	for _, numb := range numbers {
		if numb > highest {
			highest = numb
		}
		if numb < lowest {
			lowest = numb
		}
	}
	return highest + lowest
}