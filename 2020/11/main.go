package main

import (
	. "../../helpers"
	"fmt"
)

const FLOOR_CHAR = '.'
const EMPTY_CHAR = 'L'
const OCCUPIED_CHAR = '#'

func main() {
	lines := ReadFile("./2020/11/input.txt")

	rows := stringArrayToByteArray(lines)

	changeSeats(rows)
}

type row struct{
	rowNumber int
	seats []byte
}

func stringArrayToByteArray(input []string) []row{
	var output []row

	for i := 0; i < len(input); i++ {
		output = append(output, row{rowNumber: i, seats: []byte(input[i])})
	}

	return output
}

//fix deepcopy problem

func changeSeats(input []row) {

	cp := make([]row, len(input))
	copy(cp, input)

	fmt.Println(cp[0])
	fmt.Println(input[0])

	cp[0].seats[0] = OCCUPIED_CHAR
	fmt.Println(cp[0])
	fmt.Println(input[0])
}