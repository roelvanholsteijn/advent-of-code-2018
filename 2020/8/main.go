package main

import (
	. "../../helpers"
	"fmt"
	"strconv"
	"strings"
)

var n = 0
var acc = 0

func main() {
	lines := ReadFile("./2020/8/input.txt")

	var instructions []instruction

	for _, line := range lines {
		i := createInstruction(line)
		instructions = append(instructions, i)
	}

	instructionPermutations := [][]instruction{instructions}

	for i := 0; i < len(instructions); i++ {
		if instructions[i].i == "nop" && instructions[i].value < 0{
			deepCopy := append([]instruction(nil), instructions...)
			deepCopy[i].i = "jmp"
			instructionPermutations = append(instructionPermutations, deepCopy)
		} else if instructions[i].i == "jmp" {
			deepCopy := append([]instruction(nil), instructions...)
			deepCopy[i].i = "nop"
			instructionPermutations = append(instructionPermutations, deepCopy)
		}
	}

	for _, permutation := range instructionPermutations{
		acc = 0
		n = 0

		for n < len(permutation) {
			if acc > 10000{
				break
			}

			permutation[n].executeInstruction()
		}

		if acc < 10000 {
			fmt.Printf("acc: %d \n", acc)
		}
	}
}

type instruction struct {
	i string
	value int
}

func (i instruction) executeInstruction() {
	switch i.i {
	case "nop":
		n++
	case "acc":
		acc += i.value
		n++
	case "jmp":
		n += i.value
	}
}

func createInstruction(input string) instruction{
	a := strings.Split(input, " ")

	val, err := strconv.Atoi(a[1])

	if err != nil{
		fmt.Println(err)
	}

	return instruction{i: a[0], value: val}
}