package main

import (
	. "../../helpers"
	"fmt"
	"io/ioutil"
	"strings"
)


func main() {
	lines := importFile("./2020/6/input.txt")

	alphabetSeed := "abcdefghijklmnopqrstuvwxyz"
	output1, output2 := 0, 0

	for _, line := range lines {
		singleString := strings.Replace(line, "\n", "", -1)

		for _, letter := range alphabetSeed {
			amount := strings.Count(singleString, string(letter))
			if amount > 0{
				output1 ++
			}
		}
	}

	PrintOutput(1, output1)

	for _, line := range lines {
		peopleCount := len(strings.Split(line, "\n"))
		line = strings.Replace(line, "\n", "", -1)

		for _, letter := range alphabetSeed{
			if strings.Count(line, string(letter)) == peopleCount{
				output2++
			}
		}
	}

	PrintOutput(2, output2)
}

func importFile(fileName string) []string{
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}
	lines := strings.Split(string(content), "\n\n")

	return lines
}