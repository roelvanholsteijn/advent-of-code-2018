package main

import (
	. "../../helpers"
)

func part1(numbers []int)  {
	for _, i := range numbers {
		for _, j := range numbers {
			if i == j {
				continue
			}

			if i + j == 2020 {
				PrintOutput(1, i * j)
				return
			}
		}
	}
}

func part2(numbers []int) {
	for _, i := range numbers{
		for _, j := range numbers {
			if i == j {
				continue
			}

			for _, k := range numbers {
				if i == k || j == k {
					continue
				}

				if i + j + k == 2020 && (i != 0  || j != 0 || k != 0){
					PrintOutput(2, i * j * k)
					return
				}
			}
		}
	}
}

func main() {
	lines := ReadFile("./2020/1/input.txt")
	numbers := ConvertStringArrayToIntArray(lines)

	part1(numbers)
	part2(numbers)
}
