package main

import (
	. "../../helpers"
	"strconv"
	"strings"
)

var bags []bag

type bag struct {
	color string
	contains map[string]int
}

func main() {
	lines := ReadFile("./2020/7/input.txt")

	for _, i := range lines {
		bags = append(bags, createBag(i))
	}

	for _, bag := range bags {
		if bag.color == "shinygold" {
			PrintOutput(2, bag.containsAmountOfBags())
		}
	}
}

func (b bag) containsAmountOfBags() int{
	output := 0

	if len(b.contains) == 0 {
		return output
	}

	for key, value := range b.contains{
		for _, a := range bags{
			if a.color == key {
				output += (a.containsAmountOfBags() * value) + value
			}
		}
	}
	return output
}

func createBag(input string) bag{
	trimmedString := strings.Replace(input, " ", "", -1)
	bagsSplit := strings.Split(trimmedString, "bagscontain")



	var m map[string]int = make(map[string]int)

	if strings.Contains(trimmedString, "noother"){
		return bag{color:bagsSplit[0], contains: m}
	}

	a := strings.Replace(bagsSplit[1], ".", "", -1)
	a = strings.Replace(a, "bags", "", -1)
	a = strings.Replace(a, "bag", "", -1)

	b := strings.Split(a, ",")
	for _, s := range b {
		amount, _ := strconv.Atoi(s[0:1])
		m[s[1:]] = amount
	}

	return bag{color: bagsSplit[0], contains: m}
}