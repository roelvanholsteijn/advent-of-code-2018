package main

import (
	. "../../helpers"
	"sort"
)

const FRONT_CHAR = "F"
const BACK_CHAR = "B"
const LOWER_CHAR = "L"
const UPPER_CHAR = "R"

func main() {
	lines := ReadFile("./2020/5/input.txt")

	//parseSeatId(lines[0], 0, 127)
	highest := 0
	var seatIds []int
	var rows [] int

	for _, i := range lines{
		seat := parseRowSeatId(i, 0, 127)
		seat.column = parseColumnSeatId(seat.columnString, 0, 7)
		seat.seatId = seat.calculateSeatId()

		rows = append(rows, seat.row)
		seatIds = append(seatIds, seat.seatId)

		if highest < seat.seatId{
			highest = seat.seatId
		}
	}

	PrintOutput(1, highest)

	sort.Ints(seatIds[:])
	sort.Ints(rows[:])

	lowestId := (rows[0] * 8)+ 7
	highestId := rows[(len(rows) - 1)] * 8

	for i := 1; i < len(seatIds); i++ {
		if seatIds[i] < lowestId || seatIds[i] > highestId{
			continue
		}

		if seatIds[i - 1 ] + 2 == seatIds[i]{
			PrintOutput(2, seatIds[i] - 1)
		}
	}
}

type seat struct{
	row int
	column int
	columnString string
	seatId int
}

func (s seat) calculateSeatId() int {
	return (8 * s.row) + s.column
}

func parseRowSeatId(input string, start int, end int) seat{
	current := input[:1]
	seatA := seat{}
	//fmt.Printf("%s, %d, %d\n", input, start, end )

	if current != FRONT_CHAR && current != BACK_CHAR{
		seatA.row = start
		seatA.columnString = input
	} else if current == FRONT_CHAR {
		a := (end-start) / 2
		newLast := start + a

		return parseRowSeatId(input[1:], start, newLast)
	} else if current == BACK_CHAR{
		a := (end-start) / 2
		newStart := end - a

		return parseRowSeatId(input[1:], newStart, end)
	}

	return seatA
}

func parseColumnSeatId(input string, start int, end int) int{
	//fmt.Printf("%s, %d, %d\n", input, start, end )

	if input == "" {
		return start
	}
	current := input[:1]
	if current == LOWER_CHAR{
		a := (end-start) / 2
		newLast := start + a

		return parseColumnSeatId(input[1:], start, newLast)
	} else if current == UPPER_CHAR{
		a := (end-start) / 2
		newStart := end - a

		return parseColumnSeatId(input[1:], newStart, end)
	}

	return  -1
}