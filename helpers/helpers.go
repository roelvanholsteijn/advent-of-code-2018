package helpers

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func ReadFile(fileName string) []string {
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}

	lines := strings.Split(string(content), "\n")

	return lines
}

func ConvertStringArrayToIntArray(sArray []string) []int {
	var iArray []int

	for _, i := range sArray {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		iArray = append(iArray, j)
	}

	return iArray
}

func ImportIntArrayFromFile(fileName string) []int {
	file := ReadFile(fileName)

	return ConvertStringArrayToIntArray(file)
}

func ImportIntArrayArrayFromFile(filename string, sep string) [][]int {
	lines := ReadFile(filename)

	var output [][]int

	for _, line := range lines {
		var temp []int
		iArray := strings.Split(line, sep)
		for _, i := range iArray {
			iInt, _ := strconv.Atoi(i)

			temp = append(temp, iInt)
		}

		output = append(output, temp)
	}

	return output
}

func PrintOutput(partNumber int, value interface{}) {
	fmt.Print("_____\n")
	fmt.Printf("Part %d:\n", partNumber)
	fmt.Print(value)
	fmt.Print("\n_____\n")
}

func ReduceIntArray(ints []int) []int {
	var output []int

	for _, i := range ints {
		if len(output) == 0 || !IntArrayContains(output, i) {
			output = append(output, i)
		}
	}

	return output
}

func IntArrayContains(ints []int, comparable int) bool {
	for _, i := range ints {
		if i == comparable {
			return true
		}
	}

	return false
}
