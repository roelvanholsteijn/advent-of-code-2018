package main

import (
	. "../../helpers"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func main() {
	input := parseInput()

	var lowPoints []int
	var lowPointIndexes []Coordinates

	for i := 0; i < len(input); i++ {
		for j := 0; j < len(input[0]); j++ {
			if checkHorizontally(input, i, j) {
				if checkVertically(input, i, j) {
					lowPoints = append(lowPoints, input[i][j])
					temp := Coordinates{i, j}

					lowPointIndexes = append(lowPointIndexes, temp)
				}
			}
		}
	}

	sum := 0

	for _, i := range lowPoints {
		sum += i + 1
	}

	PrintOutput(1, sum)

	part2(input, lowPointIndexes)
}

func checkVertically(input [][]int, i int, j int) bool {
	if i == 0 && input[i][j] < input[i+1][j] {
		return true
	}
	if i > 0 && i < len(input)-1 {
		if input[i][j] < input[i+1][j] && input[i][j] < input[i-1][j] {
			return true
		}
	}
	if i == len(input)-1 {
		if input[i][j] < input[i-1][j] {
			return true
		}
	}

	return false
}

func checkHorizontally(input [][]int, i int, j int) bool {
	if j == 0 && input[i][j] < input[i][j+1] {
		return true
	}
	if j > 0 && j < len(input[i])-1 {
		if input[i][j] < input[i][j+1] && input[i][j] < input[i][j-1] {
			return true
		}
	}
	if j == len(input[i])-1 {
		if input[i][j] < input[i][j-1] {
			return true
		}
	}

	return false
}

func parseInput() [][]int {
	lines := ReadFile("./2021/9/input.txt")

	var input [][]int
	for _, line := range lines {
		sArray := strings.Split(line, "")

		var lineArray []int
		for _, s := range sArray {
			inputInt, _ := strconv.Atoi(s)

			lineArray = append(lineArray, inputInt)
		}

		input = append(input, lineArray)
	}

	return input
}

type Coordinates struct {
	x int
	y int
}

func part2(input [][]int, lowPointsIndexes []Coordinates) {
	var basinSizes []int

	for _, lp := range lowPointsIndexes {
		size := getBasinSize(input, lp)

		basinSizes = append(basinSizes, size)
	}

	sort.Ints(basinSizes)

	fmt.Println(basinSizes)
	fmt.Println(basinSizes[len(basinSizes)-1] * basinSizes[len(basinSizes)-2] * basinSizes[len(basinSizes)-3])
}

func getBasinSize(input [][]int, lpi Coordinates) int {
	hC := findHorizontalBasinCoordinates(input, lpi)
	vC := findVerticalBasinCoordinates(input, hC)

	return len(vC)
}

func findHorizontalBasinCoordinates(input [][]int, lpi Coordinates) []Coordinates {
	var c []Coordinates

	c = append(c, lpi)

	for i := lpi.y + 1; i < len(input[lpi.x]); i++ {
		if input[lpi.x][i] == 9 {
			break
		}
		temp := Coordinates{lpi.x, i}

		c = append(c, temp)
	}

	for i := lpi.y - 1; i >= 0; i-- {
		if input[lpi.x][i] == 9 {
			break
		}
		temp := Coordinates{lpi.x, i}

		c = append(c, temp)
	}

	return c
}

func findVerticalBasinCoordinates(input [][]int, hc []Coordinates) []Coordinates {
	c := findVerticalCoordinates(input, hc)

	var allC []Coordinates
	for _, coordinate := range c {
		temp := findHorizontalBasinCoordinates(input, coordinate)
		allC = append(allC, temp...)
	}

	vc := findVerticalCoordinates(input, allC)

	return reduceCoordinateArray(vc)
}

func findVerticalCoordinates(input [][]int, hc []Coordinates) []Coordinates {
	c := hc

	for _, coordinate := range hc {
		for i := coordinate.x + 1; i < len(input); i++ {
			if input[i][coordinate.y] == 9 {
				break
			}

			temp := Coordinates{i, coordinate.y}

			c = append(c, temp)
		}

		for i := coordinate.x - 1; i >= 0; i-- {
			if input[i][coordinate.y] == 9 {
				break
			}
			temp := Coordinates{i, coordinate.y}

			c = append(c, temp)
		}
	}

	return c
}

func reduceCoordinateArray(coordinates []Coordinates) []Coordinates {
	var output []Coordinates

	for _, c := range coordinates {
		if len(output) == 0 {
			output = append(output, c)
			continue
		}

		if !coordinateArrayContainsCoordinate(output, c) {
			output = append(output, c)
		}
	}

	return output
}

func coordinateArrayContainsCoordinate(coordinates []Coordinates, c Coordinates) bool {
	for _, coordinate := range coordinates {
		if c.x == coordinate.x && c.y == coordinate.y {
			return true
		}
	}
	return false
}
