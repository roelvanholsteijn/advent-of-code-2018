package main

import (
	. "../../helpers"
	"strconv"
	"strings"
)

func main() {
	lines := ReadFile("./2021/2/input.txt")

	instructions := createInstructionArray(lines)

	calculatePosition(instructions)
	calculatePositionWithAim(instructions)
}

const FORWARD = "forward"
const UP = "up"
const DOWN = "down"

type instruction struct {
	direction string
	amount    int
}

func createInstructionArray(input []string) []instruction {
	var instructions []instruction

	for _, i := range input {
		s := strings.Split(i, " ")
		num, _ := strconv.Atoi(s[1])
		i := instruction{direction: s[0], amount: num}

		instructions = append(instructions, i)
	}

	return instructions
}

func calculatePosition(input []instruction) {
	x := 0
	z := 0

	for _, i := range input {
		switch i.direction {
		case FORWARD:
			x += i.amount
		case DOWN:
			z += i.amount
		case UP:
			z -= i.amount
		}
	}

	PrintOutput(1, x*z)
}

func calculatePositionWithAim(input []instruction) {
	aim := 0
	x := 0
	z := 0

	for _, i := range input {
		switch i.direction {
		case FORWARD:
			x += i.amount
			z += aim * i.amount
		case DOWN:
			aim += i.amount
		case UP:
			aim -= i.amount
		}
	}

	PrintOutput(2, x*z)
}
