package main

import (
	. "../../helpers"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	coordinates, folds := parseInput()

	fold(coordinates, folds)
}

type Coordinate struct {
	x int
	y int
}

type Fold struct {
	isHorizontal bool
	amount       int
}

func parseInput() ([]Coordinate, []Fold) {
	lines := readFileTwoParts("./2021/13/input.txt")

	coordinates := parseCoordinates(lines[0])
	folds := parseFolds(lines[1])

	return coordinates, folds
}

func parseCoordinates(input string) []Coordinate {
	lines := strings.Split(input, "\n")

	var output []Coordinate

	for _, line := range lines {
		c := strings.Split(line, ",")
		x, _ := strconv.Atoi(c[0])
		y, _ := strconv.Atoi(c[1])
		coord := Coordinate{x, y}

		output = append(output, coord)
	}

	return output
}

func parseFolds(input string) []Fold {
	lines := strings.Split(input, "\n")

	var output []Fold

	for _, line := range lines {
		temp := strings.Split(line, " along ")
		s := strings.Split(temp[1], "=")
		whereI, _ := strconv.Atoi(s[1])

		isHorizontal := false
		if s[0] == "y" {
			isHorizontal = true
		}

		fold := Fold{isHorizontal, whereI}
		output = append(output, fold)
	}

	return output
}

func readFileTwoParts(fileName string) []string {
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}

	lines := strings.Split(string(content), "\n\n")

	return lines
}

func foldDict(input map[int][]int, fold Fold) map[int][]int {
	foldedDict := make(map[int][]int)

	for k, v := range input {
		for _, value := range v {
			if value > fold.amount-1 {
				temp := value - fold.amount - 1

				pos := fold.amount - 1 - temp

				tempA := foldedDict[k]
				foldedDict[k] = append(tempA, pos)
			} else {
				tempA := foldedDict[k]
				foldedDict[k] = append(tempA, value)
			}
		}
	}

	return foldedDict
}

func foldVertical(coordinates []Coordinate, fold Fold) []Coordinate {
	dictY := make(map[int][]int)

	for _, c := range coordinates {
		dictY[c.y] = append(dictY[c.y], c.x)
	}

	foldedDict := foldDict(dictY, fold)

	var output []Coordinate

	for k, v := range foldedDict {
		temp := ReduceIntArray(v)

		for _, value := range temp {
			c := Coordinate{value, k}

			output = append(output, c)
		}
	}

	return output
}

func foldHorizontal(coordinates []Coordinate, fold Fold) []Coordinate {
	dictX := make(map[int][]int)

	for _, c := range coordinates {
		dictX[c.x] = append(dictX[c.x], c.y)
	}

	foldedDict := foldDict(dictX, fold)

	var output []Coordinate

	for k, v := range foldedDict {
		temp := ReduceIntArray(v)

		for _, value := range temp {
			c := Coordinate{k, value}

			output = append(output, c)
		}
	}
	return output
}

func printCoordinateList(coordinates []Coordinate) {
	c := getHighestCoordinate(coordinates)

	var output [][]int

	for i := 0; i <= c.y; i++ {
		var temp []int
		for j := 0; j <= c.x; j++ {
			temp = append(temp, 0)
		}
		output = append(output, temp)
	}

	for _, coord := range coordinates {
		output[coord.y][coord.x] = 1
	}

	for i, _ := range output {
		for _, c := range output[i] {
			if c == 1 {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Print("\n")
	}
}

func getHighestCoordinate(coordinate []Coordinate) Coordinate {
	highestX := 0
	highestY := 0

	for _, c := range coordinate {
		if c.x > highestX {
			highestX = c.x
		}

		if c.y > highestY {
			highestY = c.y
		}
	}

	t := Coordinate{highestX, highestY}

	return t
}

func fold(coordinates []Coordinate, folds []Fold) {
	output := coordinates

	for _, f := range folds {
		if f.isHorizontal {
			output = foldHorizontal(output, f)
		} else {
			output = foldVertical(output, f)
		}
	}

	printCoordinateList(output)
}
