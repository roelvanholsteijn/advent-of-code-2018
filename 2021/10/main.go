package main

import (
	. "../../helpers"
	"fmt"
	"sort"
	"strings"
)

func main() {
	part1()
	part2()
}

type chunk struct {
	string
	chunks []chunk
}

func part1() {
	lines := ReadFile("./2021/10/input.txt")

	sum := 0

	for _, line := range lines {
		temp := line

		for {
			tLen := len(temp)
			temp = strings.ReplaceAll(temp, "<>", "")
			temp = strings.ReplaceAll(temp, "()", "")
			temp = strings.ReplaceAll(temp, "{}", "")
			temp = strings.ReplaceAll(temp, "[]", "")

			if len(temp) == tLen {
				break
			}
		}

		temp = strings.ReplaceAll(temp, "{", "")
		temp = strings.ReplaceAll(temp, "<", "")
		temp = strings.ReplaceAll(temp, "(", "")
		temp = strings.ReplaceAll(temp, "[", "")

		if len(temp) > 0 {
			switch temp[0:1] {
			case ")":
				sum += 3
			case "]":
				sum += 57
			case "}":
				sum += 1197
			case ">":
				sum += 25137
			}
		}
	}

	fmt.Println(sum)
}

func part2() {
	lines := ReadFile("./2021/10/input.txt")

	var incomplete []string

	for _, line := range lines {
		temp := line

		for {
			tLen := len(temp)
			temp = strings.ReplaceAll(temp, "<>", "")
			temp = strings.ReplaceAll(temp, "()", "")
			temp = strings.ReplaceAll(temp, "{}", "")
			temp = strings.ReplaceAll(temp, "[]", "")

			if len(temp) == tLen {
				break
			}
		}

		temp2 := strings.ReplaceAll(temp, "{", "")
		temp2 = strings.ReplaceAll(temp2, "<", "")
		temp2 = strings.ReplaceAll(temp2, "(", "")
		temp2 = strings.ReplaceAll(temp2, "[", "")

		if len(temp2) == 0 {
			incomplete = append(incomplete, temp)
		}
	}

	var completion []string
	for _, i := range incomplete {
		r := []rune(i)
		c := ""

		r = reverseRuneArray(r)

		for j := 0; j < len(r); j++ {
			switch string(r[j]) {
			case "[":
				c = c + "]"
			case "(":
				c = c + ")"
			case "{":
				c = c + "}"
			case "<":
				c = c + ">"
			}
		}
		completion = append(completion, c)
	}

	var scores []int

	for _, c := range completion {
		s := calculateScore(c)

		scores = append(scores, s)
	}

	sort.Ints(scores)

	fmt.Println(scores[len(scores)/2])
}

func reverseRuneArray(r []rune) []rune {
	for i, j := 0, len(r)-1; i < j; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}

	return r
}

func calculateScore(input string) int {
	r := []rune(input)
	score := 0

	for _, ru := range r {
		score = score * 5

		switch string(ru) {
		case ")":
			score += 1
		case "]":
			score += 2
		case "}":
			score += 3
		case ">":
			score += 4
		}
	}

	return score
}
