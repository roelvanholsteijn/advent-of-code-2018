package main

import (
	. "../../helpers"
	"strconv"
	"strings"
)

func main() {
	part1()
	part2()
}

func absoluteNumber(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func parseInput(lines []string) ([]int, int) {
	hPos := strings.Split(lines[0], ",")

	var crabs []int
	highest := 0

	for _, c := range hPos {
		hPosInt, _ := strconv.Atoi(c)

		if hPosInt > highest {
			highest = hPosInt
		}

		crabs = append(crabs, hPosInt)
	}

	return crabs, highest
}

func part1() {
	lines := ReadFile("./2021/7/input.txt")
	crabs, highestInt := parseInput(lines)

	lowestFuel := 0

	for i := 0; i < highestInt; i++ {
		fuel := 0

		for _, c := range crabs {
			temp := absoluteNumber(c - i)

			fuel += temp
		}

		if i == 0 {
			lowestFuel = fuel
		} else {
			if lowestFuel > fuel {
				lowestFuel = fuel
			}
		}
	}

	PrintOutput(1, lowestFuel)
}

func part2() {
	lines := ReadFile("./2021/7/input.txt")
	crabs, highestInt := parseInput(lines)

	lowestFuel := 0

	for i := 0; i < highestInt; i++ {
		fuel := 0

		for _, c := range crabs {
			temp := absoluteNumber(c - i)
			for i := 0; i <= temp; i++ {
				fuel += i
			}
		}

		if i == 0 {
			lowestFuel = fuel
		} else {
			if lowestFuel > fuel {
				lowestFuel = fuel
			}
		}
	}

	PrintOutput(2, lowestFuel)
}