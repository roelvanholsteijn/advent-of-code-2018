package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strings"
)

func main() {
	file := readFileTwoParts("./2021/14/input.txt")
	template := file[0]
	lines := strings.Split(file[1], "\n")
	pairInsertions := parsePairInsertions(lines)
	pairsDict := makePairDict(pairInsertions)
	pairCount, letterCount := makePairAndLetterCount(template)

	steps := 40

	for i := 0; i < steps; i++ {
		pairCount, letterCount = insertElements(pairsDict, pairCount, letterCount)
	}

	highest, lowest := 0, math.MaxInt

	for _, c := range letterCount {
		if c > highest {
			highest = c
		}
		if c < lowest {
			lowest = c
		}
	}

	fmt.Println(highest - lowest)
}

func readFileTwoParts(fileName string) []string {
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}

	lines := strings.Split(string(content), "\n\n")

	return lines
}

type PairInsertion struct {
	pair        string
	newPair1    string
	newPair2    string
	addedLetter string
}

func parsePairInsertions(input []string) []PairInsertion {
	var output []PairInsertion

	for _, line := range input {
		s := strings.Split(line, " -> ")

		n := strings.Split(s[0], "")

		newPair1 := n[0] + s[1]
		newPair2 := s[1] + n[1]

		temp := PairInsertion{s[0], newPair1, newPair2, s[1]}
		output = append(output, temp)
	}

	return output
}

func insertElements(pairs map[string]PairInsertion, pairCount map[string]int, letterCount map[string]int) (map[string]int, map[string]int) {
	pc := make(map[string]int)

	for k, v := range pairCount {
		pc[k] = v
	}

	for p, _ := range pairCount {
		pCount := pairCount[p]
		pair := pairs[p]
		pc[pair.newPair1] += pCount
		pc[pair.newPair2] += pCount
		pc[pair.pair] -= pCount
		letterCount[pair.addedLetter] += pCount
	}

	return pc, letterCount
}

func makePairDict(pairs []PairInsertion) map[string]PairInsertion {
	dict := make(map[string]PairInsertion)

	for _, p := range pairs {
		dict[p.pair] = p
	}

	return dict
}

func makePairAndLetterCount(template string) (map[string]int, map[string]int) {
	pairCount := makePairCount(template)
	letterCount := makeLetterCount(template)

	return pairCount, letterCount
}

func makeLetterCount(template string) map[string]int {
	dict := make(map[string]int)
	for i := 0; i < len(template); i++ {
		dict[template[i:i+1]] += 1
	}

	return dict
}

func makePairCount(template string) map[string]int {
	dict := make(map[string]int)

	for i := 0; i < len(template)-1; i++ {
		dict[template[i:i+2]] += 1
	}

	return dict
}
