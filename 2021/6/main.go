package main

import (
	. "../../helpers"
	"strconv"
	"strings"
)

func main() {
	lines := ReadFile("./2021/6/input.txt")

	school1 := createSchool(lines)
	school2 := createSchool(lines)

	days1 := 80
	days2 := 256

	for i := 0; i < days1; i++ {
		school1 = school1.nextDay()
	}

	for i := 0; i < days2; i++ {
		school2 = school2.nextDay()
	}

	PrintOutput(1, school1.getTotalFishes())
	PrintOutput(2, school2.getTotalFishes())
}

func parseInput(lines []string) []int {
	fishS := strings.Split(lines[0], ",")
	var fish []int

	for _, f := range fishS {
		fishI, _ := strconv.Atoi(f)

		fish = append(fish, fishI)
	}

	return fish
}

type School struct {
	n0 int
	n1 int
	n2 int
	n3 int
	n4 int
	n5 int
	n6 int
	n7 int
	n8 int
}

func (s School) nextDay() School {
	day6 := s.n7 + s.n0

	tempS := School{
		n0: s.n1,
		n1: s.n2,
		n2: s.n3,
		n3: s.n4,
		n4: s.n5,
		n5: s.n6,
		n6: day6,
		n7: s.n8,
		n8: s.n0,
	}

	return tempS
}

func (s School) getTotalFishes() int {
	return s.n0 + s.n1 + s.n2 + s.n3 + s.n4 + s.n5 + s.n6 + s.n7 + s.n8
}

func createSchool(lines []string) School {
	fishes := parseInput(lines)

	school := School{}
	for _, f := range fishes {
		switch f {
		case 1:
			school.n1 += 1
		case 2:
			school.n2 += 1
		case 3:
			school.n3 += 1
		case 4:
			school.n4 += 1
		case 5:
			school.n5 += 1
		case 6:
			school.n6 += 1
		case 7:
			school.n7 += 1
		}
	}

	return school
}
