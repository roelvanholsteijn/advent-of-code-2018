package main

import (
	. "../../helpers"
	"strconv"
	"strings"
)

func main() {
	lines := ReadFile("./2021/5/input.txt")

	l := parseInputLines(lines)

	var part1Lines []Line
	var part2Lines []Line

	for _, line := range l {
		if line.isStraitLine() {
			temp := line.calculateCoordinates()

			part1Lines = append(part1Lines, temp)
		}
		if line.isStraitLine() || line.isDiagonalLine() {
			temp := line.calculateCoordinates()

			part2Lines = append(part2Lines, temp)
		}
	}

	PrintOutput(1, calculateIntersections(part1Lines))
	PrintOutput(1, calculateIntersections(part2Lines))
}

func parseInputLines(lines []string) []Line {
	var l []Line

	for _, line := range lines {
		split := strings.Split(line, " -> ")
		start := strings.Split(split[0], ",")
		end := strings.Split(split[1], ",")

		xStart, _ := strconv.Atoi(start[0])
		xEnd, _ := strconv.Atoi(end[0])
		yStart, _ := strconv.Atoi(start[1])
		yEnd, _ := strconv.Atoi(end[1])

		temp := Line{
			XStart: xStart,
			XEnd:   xEnd,
			YStart: yStart,
			YEnd:   yEnd,
		}

		l = append(l, temp)
	}

	return l
}

type Coordinates struct {
	x int
	y int
}

type Line struct {
	XStart     int
	XEnd       int
	YStart     int
	YEnd       int
	GridPoints []Coordinates
}

func (l Line) isStraitLine() bool {
	return (l.XStart == l.XEnd && l.YStart != l.YEnd) || (l.XStart != l.XEnd && l.YStart == l.YEnd)
}

func (l Line) isDiagonalLine() bool {
	return !l.isStraitLine()
}

func (l Line) calculateCoordinates() Line {
	if l.isDiagonalLine() {
		return l.calculateCoordinatesDiagonalLine()
	} else {
		return l.calculateCoordinatesStraitLine()
	}
}

func (l Line) calculateCoordinatesStraitLine() Line {
	var gridPoints []Coordinates

	if l.XEnd == l.XStart {
		var yStart int
		var yEnd int

		if l.YEnd-l.YStart > 0 {
			yStart = l.YStart
			yEnd = l.YEnd
		} else {
			yStart = l.YEnd
			yEnd = l.YStart
		}

		for i := yStart; i <= yEnd; i++ {
			temp := Coordinates{
				x: l.XStart,
				y: i,
			}

			gridPoints = append(gridPoints, temp)
		}
	} else {
		var xStart int
		var xEnd int

		if l.XEnd-l.XStart > 0 {
			xStart = l.XStart
			xEnd = l.XEnd
		} else {
			xStart = l.XEnd
			xEnd = l.XStart
		}

		for i := xStart; i <= xEnd; i++ {
			temp := Coordinates{
				x: i,
				y: l.YStart,
			}

			gridPoints = append(gridPoints, temp)
		}
	}

	l.GridPoints = gridPoints

	return l
}

func (l Line) calculateCoordinatesDiagonalLine() Line {
	var gridPoints []Coordinates

	if l.XStart-l.XEnd == l.YStart-l.YEnd {
		var yStart int
		var xStart int
		var yEnd int

		if l.YEnd-l.YStart > 0 {
			xStart = l.XStart
			yStart = l.YStart
			yEnd = l.YEnd
		} else {
			xStart = l.XEnd
			yStart = l.YEnd
			yEnd = l.YStart
		}

		counter := 0

		for i := yStart; i <= yEnd; i++ {
			temp := Coordinates{
				x: xStart + counter,
				y: i,
			}

			counter++

			gridPoints = append(gridPoints, temp)
		}
	} else {
		if l.XStart > l.XEnd {
			xTemp := l.XStart
			for i := l.YStart; i <= l.YEnd; i++ {
				temp := Coordinates{
					x: xTemp,
					y: i,
				}

				xTemp--
				gridPoints = append(gridPoints, temp)
			}
		} else {
			yTemp := l.YStart
			for i := l.XStart; i <= l.XEnd; i++ {
				temp := Coordinates{
					x: i,
					y: yTemp,
				}

				yTemp--
				gridPoints = append(gridPoints, temp)
			}
		}
	}

	l.GridPoints = gridPoints

	return l
}

func calculateIntersections(lines []Line) int {
	m := map[string]int{}

	for _, line := range lines {
		for _, gp := range line.GridPoints {
			x := strconv.Itoa(gp.x)
			y := strconv.Itoa(gp.y)

			xyString := x + "," + y

			_, ok := m[xyString]

			if ok {
				m[xyString] += 1
			} else {
				m[xyString] = 1
			}
		}
	}

	counter := 0

	for _, value := range m {
		if value > 1 {
			counter++
		}
	}

	return counter
}
