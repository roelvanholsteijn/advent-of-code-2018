package main

import (
	. "../../helpers"
	"sort"
	"strconv"
	"strings"
)

func main() {
	segments := parseInput()

	total := 0
	for _, s := range segments {

		counter := 0

		for _, sAll := range s.Used {
			switch len(sAll) {
			case 2:
				counter++
			case 3:
				counter++
			case 4:
				counter++
			case 7:
				counter++
			}
		}

		total += counter
	}

	PrintOutput(1, total)

	part2()
}

func parseInput() []Segments {
	lines := ReadFile("./2021/8/input.txt")

	var input []Segments

	for _, line := range lines {
		s := strings.Split(line, " | ")

		allSignalPatterns := strings.Split(s[0], " ")
		usedSignalPatterns := strings.Split(s[1], " ")

		for i := 0; i < len(allSignalPatterns); i++ {
			allSignalPatterns[i] = strings.TrimSpace(allSignalPatterns[i])
			allSignalPatterns[i] = sortStringByChar(allSignalPatterns[i])
		}

		for i := 0; i < len(usedSignalPatterns); i++ {
			usedSignalPatterns[i] = strings.TrimSpace(usedSignalPatterns[i])
			usedSignalPatterns[i] = sortStringByChar(usedSignalPatterns[i])
		}

		temp := Segments{allSignalPatterns, usedSignalPatterns}

		input = append(input, temp)
	}

	return input
}

func sortStringByChar(s string) string {
	var runeS []rune
	for _, r := range s {
		runeS = append(runeS, r)
	}

	sort.Slice(runeS, func(i, j int) bool {
		return runeS[i] < runeS[j]
	})

	return string(runeS)
}

type Segments struct {
	All  []string
	Used []string
}

type Display struct {
	zero  string
	one   string
	two   string
	three string
	four  string
	five  string
	six   string
	seven string
	eight string
	nine  string
}

func part2() {
	segments := parseInput()

	sum := 0

	for _, s := range segments {
		d := Display{}

		for _, sAll := range s.All {
			switch len(sAll) {
			case 2:
				d.one = sAll
			case 3:
				d.seven = sAll
			case 4:
				d.four = sAll
			case 7:
				d.eight = sAll
			}
		}

		d.determineOtherLetters(s)

		var number string

		for _, sUsed := range s.Used {
			switch sUsed {
			case d.zero:
				number += "0"
			case d.one:
				number += "1"
			case d.two:
				number += "2"
			case d.three:
				number += "3"
			case d.four:
				number += "4"
			case d.five:
				number += "5"
			case d.six:
				number += "6"
			case d.seven:
				number += "7"
			case d.eight:
				number += "8"
			case d.nine:
				number += "9"
			}
		}

		i, _ := strconv.Atoi(number)

		sum += i
	}

	PrintOutput(2, sum)

}

func (d *Display) determineOtherLetters(s Segments) {
	r := []rune(d.one)

	fiveLines := strings.ReplaceAll(d.four, string(r[0]), "")
	fiveLines = strings.ReplaceAll(fiveLines, string(r[1]), "")

	for _, segment := range s.All {
		if isNine(d, segment) {
			d.nine = segment
		}
	}

	bottomLine := determineBottomLine(d)
	fiveLines += bottomLine
	fiveLines = sortStringByChar(fiveLines)

	var middleLine string
	for _, segment := range s.All {
		if len(segment) == 5 {
			r := []rune(d.seven)

			temp := segment
			temp = strings.ReplaceAll(temp, string(r[0]), "")
			temp = strings.ReplaceAll(temp, string(r[1]), "")
			temp = strings.ReplaceAll(temp, string(r[2]), "")

			if temp == fiveLines {
				d.five = segment
			} else {
				rb := []rune(bottomLine)
				temp = strings.ReplaceAll(temp, string(rb[0]), "")
				if len(temp) == 1 {
					d.three = segment
					middleLine = temp
				} else {
					d.two = segment
				}
			}
		}
	}

	for _, segment := range s.All {
		if len(segment) == 6 && segment != d.nine {
			r := []rune(middleLine)

			temp := strings.ReplaceAll(segment, string(r), "")

			if len(temp) == 5 {
				d.six = segment
			} else {
				d.zero = segment
			}
		}
	}
}

func isNine(d *Display, s string) bool {
	if len(s) == 6 {
		r := []rune(d.one)

		fiveLines := strings.ReplaceAll(d.four, string(r[0]), "")
		fiveLines = strings.ReplaceAll(fiveLines, string(r[1]), "")
		sevenLines := []rune(d.seven)
		fiveLinesR := []rune(fiveLines)

		temp := s

		temp = strings.ReplaceAll(temp, string(sevenLines[0]), "")
		temp = strings.ReplaceAll(temp, string(sevenLines[1]), "")
		temp = strings.ReplaceAll(temp, string(sevenLines[2]), "")
		temp = strings.ReplaceAll(temp, string(fiveLinesR[0]), "")
		temp = strings.ReplaceAll(temp, string(fiveLinesR[1]), "")

		if len(temp) == 1 {
			return true
		}
	}

	return false
}

func determineBottomLine(d *Display) string {
	r := []rune(d.one)

	fiveLines := strings.ReplaceAll(d.four, string(r[0]), "")
	fiveLines = strings.ReplaceAll(fiveLines, string(r[1]), "")
	sevenLines := []rune(d.seven)
	fiveLinesR := []rune(fiveLines)

	temp := d.nine

	temp = strings.ReplaceAll(temp, string(sevenLines[0]), "")
	temp = strings.ReplaceAll(temp, string(sevenLines[1]), "")
	temp = strings.ReplaceAll(temp, string(sevenLines[2]), "")
	temp = strings.ReplaceAll(temp, string(fiveLinesR[0]), "")
	temp = strings.ReplaceAll(temp, string(fiveLinesR[1]), "")

	return temp
}
