##Advent of code 2021
###Day 1 - Warm up
This day was an easy puzzle to get back into it. My init script failed, so I need to fix that, but otherwise both parts where pretty strait forward.
###Day 2
Today I when I saw the puzzle I decided to start by creating a struct to put the instruction in. In order to make sure I didnt get confused by them down the line. This turned out to be a bit overkill. Part 2 was an easy adjustment to Part 1.
###Day 3 - First Fail
Today I had my first problems. At part one it took me 10 minutes to find how to convert a bitstring to an int. And in part two it took me half an hour to figure out I had not added an exit condition to a loop. Well... It happens.
###Day 4 
This one took some time. I started out not using structs for some reason and made it a mess. Then I rewrote the entire thing. The test_input does not work for part 2 but the actual one does. Maybe I figure out why later.
###Day 5
Reading is to difficult
###Day 6 
Got part 1 in about 20 minutes and spent about an hour trying to find out if int arrays can get a max size of int64 instead of int32. Then rewrote the entire thing again
###Day 7
I was still a bit asleep but didn't make any mistakes because of it. It just took a bit longer than normal and was still quick enough.