package main

import (
	. "../../helpers"
	"fmt"
	"strconv"
)

func main() {
	lines := ReadFile("./2021/3/input.txt")

	getMostUsedDigit(lines)
	o2 := filterO2Rating(lines)
	co2 := filterCO2Rating(lines)

	fmt.Println(o2)
	fmt.Println(co2)
	//
	PrintOutput(2, o2*co2)
}

func getMostUsedDigit(input []string) {
	mostUsedString := ""
	leastUsedString := ""

	length := len(input[0])

	for i := 0; i < length; i++ {
		zeroCounter := 0
		oneCounter := 0

		for _, j := range input {
			char := j[i : i+1]
			if char == "0" {
				zeroCounter += 1
			}
			if char == "1" {
				oneCounter += 1
			}
		}

		if zeroCounter > oneCounter {
			mostUsedString += "0"
			leastUsedString += "1"
		} else {
			mostUsedString += "1"
			leastUsedString += "0"
		}
	}
	gamma, _ := strconv.ParseInt(mostUsedString, 2, 64)
	epsilon, _ := strconv.ParseInt(leastUsedString, 2, 64)

	PrintOutput(1, gamma*epsilon)
}

func filterO2Rating(input []string) int64 {

	var o2RatingStrings = input

	length := len(input[0])

	for i := 0; i < length; i++ {
		zeroCounter := 0
		oneCounter := 0

		if len(o2RatingStrings) == 1 {
			continue
		}

		for _, j := range o2RatingStrings {
			char := j[i : i+1]
			if char == "0" {
				zeroCounter += 1
			}
			if char == "1" {
				oneCounter += 1
			}
		}

		if zeroCounter > oneCounter {
			var temp []string
			for _, value := range o2RatingStrings {
				char := value[i : i+1]

				if char == "0" {
					temp = append(temp, value)
				}
			}

			o2RatingStrings = temp
		} else {
			var temp []string
			for _, value := range o2RatingStrings {
				char := value[i : i+1]

				if char == "1" {
					temp = append(temp, value)
				}
			}
			o2RatingStrings = temp
		}
	}

	fmt.Println(o2RatingStrings)

	o2Rating, _ := strconv.ParseInt(o2RatingStrings[0], 2, 64)

	return o2Rating
}

func filterCO2Rating(input []string) int64 {

	var cO2RatingStrings = input

	length := len(input[0])

	for i := 0; i < length; i++ {
		zeroCounter := 0
		oneCounter := 0

		if len(cO2RatingStrings) == 1 {
			continue
		}

		for _, j := range cO2RatingStrings {
			char := j[i : i+1]
			if char == "0" {
				zeroCounter += 1
			}
			if char == "1" {
				oneCounter += 1
			}
		}

		if oneCounter < zeroCounter {
			var temp []string
			for _, value := range cO2RatingStrings {
				char := value[i : i+1]

				if char == "1" {
					temp = append(temp, value)
				}
			}
			cO2RatingStrings = temp
		} else {
			var temp []string
			for _, value := range cO2RatingStrings {
				char := value[i : i+1]

				if char == "0" {
					temp = append(temp, value)
				}
			}
			cO2RatingStrings = temp
		}
	}

	fmt.Println(cO2RatingStrings)

	cO2Rating, _ := strconv.ParseInt(cO2RatingStrings[0], 2, 64)

	return cO2Rating
}
