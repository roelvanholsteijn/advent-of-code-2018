package main

import (
	. "../../helpers"
	"fmt"
	"strings"
)

func main() {
	nodes := parseInput()

	calculateSteps(nodes)
}

type Node struct {
	name            string
	connectingNodes []string
}

func parseInput() []Node {
	lines := ReadFile("./2021/12/input.txt")

	var output []Node

	for _, line := range lines {
		s := strings.Split(line, "-")

		if nodeArrayContainsName(output, s[0]) {
			for i, n := range output {
				if n.name == s[0] && !arrayContainsString(n.connectingNodes, s[1]) {
					output[i].connectingNodes = append(output[i].connectingNodes, s[1])
				}
			}
		}
		if nodeArrayContainsName(output, s[1]) {
			for i, n := range output {
				if n.name == s[1] && !arrayContainsString(n.connectingNodes, s[0]) {
					output[i].connectingNodes = append(output[i].connectingNodes, s[0])
				}
			}
		}
		if !nodeArrayContainsName(output, s[0]) {
			temp := Node{s[0], nil}
			temp.connectingNodes = append(temp.connectingNodes, s[1])

			output = append(output, temp)
		}
		if !nodeArrayContainsName(output, s[1]) {
			temp := Node{s[1], nil}
			temp.connectingNodes = append(temp.connectingNodes, s[0])

			output = append(output, temp)
		}
	}

	return output
}

func arrayContainsString(input []string, comparable string) bool {
	for _, s := range input {
		if s == comparable {
			return true
		}
	}

	return false
}

func nodeArrayContainsName(input []Node, comparable string) bool {
	for _, n := range input {
		if n.name == comparable {
			return true
		}
	}

	return false
}

func calculateSteps(nodes []Node) {

	//for _, n := range nodes {
	//	fmt.Println(n)
	//}

	output := calculateNextStepNew(nodes, []string{}, "start", nil)

	//for _, o := range output {
	//	fmt.Println(o)
	//}

	fmt.Println(len(output))
}

func calculateNextStep(nodes []Node, route []string, output [][]string) [][]string {
out:

	//getNode()
	for _, n := range nodes {
		if n.name == route[len(route)-1] {
			for _, nextNode := range n.connectingNodes {
				if nextNode == "end" {
					temp := append(route, nextNode)

					output = append(output, temp)
					break out
				}

				if nextNode != strings.ToLower(nextNode) || !arrayContainsString(route, nextNode) {
					temp := append(route, nextNode)
					output = calculateNextStep(nodes, temp, output)
				}

			}
		}
	}

	return output
}

func calculateNextStepNew(nodes []Node, route []string, origin string, output [][]string) [][]string {
	if origin == "end" {
		output = append(output, route)
		return output
	}

	if checkPartTwo(route, origin) || origin == strings.ToUpper(origin) {
		currentNode := getNodeFromArray(nodes, origin)

		temp := append(route, origin)
		for _, connecting := range currentNode.connectingNodes {
			output = calculateNextStepNew(nodes, temp, connecting, output)
		}
	}

	return output
}

func checkPartTwo(route []string, origin string) bool {
	if origin == strings.ToLower(origin) {
		temp := route

		count := getCount(temp)

		if origin == "start" && count[origin] == 1 {
			return false
		}

		if !dictContainsTwo(count) || (dictContainsTwo(count) && count[origin] == 0) {
			return true
		}

	}

	return false
}

func getNodeFromArray(nodes []Node, name string) Node {
	for _, n := range nodes {
		if n.name == name {
			return n
		}
	}

	return Node{}
}

func getCount(s []string) map[string]int {
	dict := make(map[string]int)

	for _, str := range s {
		dict[str] = dict[str] + 1
	}

	return dict
}

func dictContainsTwo(d map[string]int) bool {
	for k, v := range d {
		if k == strings.ToUpper(k) {
			continue
		}
		if v == 2 {
			return true
		}
	}
	return false
}

func removeElementFromArray(slice []string, i int) []string {
	return append(slice[:i], slice[i+1:]...)
}
