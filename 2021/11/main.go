package main

import (
	. "../../helpers"
	"fmt"
	"strconv"
	"strings"
)

var flashes int

func main() {
	octo := ImportIntArrayArrayFromFile("./2021/11/input.txt", "")

	flashes = 0
	steps := 0
	temp := 0

	for {
		temp = flashes
		octo = nextStep(octo)
		steps += 1

		if flashes-temp == 100 {
			break
		}
	}

	for _, l := range octo {
		fmt.Println(l)
	}

	fmt.Println(flashes)
	fmt.Println(steps)

}

func parseInput() [][]int {
	lines := ReadFile("./2021/11/input.txt")

	var octo [][]int

	for _, line := range lines {
		var temp []int
		oArray := strings.Split(line, "")
		for _, o := range oArray {
			oInt, _ := strconv.Atoi(o)

			temp = append(temp, oInt)
		}

		octo = append(octo, temp)
	}

	return octo
}

func flash(octo [][]int, coordinate Coordinate) [][]int {
	coordinates := coordinate.getAdjacentCoordinates()

	flashes += 1
	octo[coordinate.x][coordinate.y] = -1

	for _, c := range coordinates {
		if octo[c.x][c.y] != -1 {
			octo[c.x][c.y] += 1
		}

		if octo[c.x][c.y] > 9 {
			octo = flash(octo, c)
		}
	}

	return octo
}

type Coordinate struct {
	x int
	y int
}

func (c Coordinate) getAdjacentCoordinates() []Coordinate {
	var coordinates []Coordinate

	for i := c.x - 1; i <= c.x+1; i++ {
		for j := c.y - 1; j <= c.y+1; j++ {
			if i == c.x && j == c.y {
				continue
			}
			if i < 0 || i > 9 || j < 0 || j > 9 {
				continue
			}

			temp := Coordinate{i, j}

			coordinates = append(coordinates, temp)
		}
	}

	return coordinates
}

func nextStep(octo [][]int) [][]int {
	for i := 0; i < len(octo); i++ {
		for j := 0; j < len(octo[i]); j++ {
			octo[i][j] += 1
		}
	}

	for i := 0; i < len(octo); i++ {
		for j := 0; j < len(octo[i]); j++ {
			if octo[i][j] > 9 {
				octo = flash(octo, Coordinate{i, j})
			}
		}
	}

	for i := 0; i < len(octo); i++ {
		for j := 0; j < len(octo[i]); j++ {
			if octo[i][j] == -1 {
				octo[i][j] = 0
			}
		}
	}

	return octo
}
