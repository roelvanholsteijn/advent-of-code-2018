package main

import (
	. "../../helpers"
	"fmt"
	"io/ioutil"
	"strings"
)

type DictValue struct {
	index int
	value bool
}

type Board struct {
	Lines              []map[int]DictValue
	WinningNumber      int
	WinningNumberIndex int
}

func main() {
	lines := readFile("./2021/4/input.txt")

	bingoNumbers, boards := parseInput(lines)

	calculateBoards(bingoNumbers, boards)
}

func parseInput(lines []string) ([]int, []Board) {
	splitNumbers := strings.Split(lines[0], ",")
	bingoNumbers := ConvertStringArrayToIntArray(splitNumbers)

	var boards []Board
	for i := 1; i < len(lines); i++ {
		splitBoard := strings.Split(lines[i], "\n")
		var board [][]int
		for _, value := range splitBoard {
			value = strings.Replace(value, "  ", " ", -1)
			value = strings.TrimLeft(value, " ")
			splitNum := strings.Split(value, " ")
			for _, n := range splitNum {
				n = strings.TrimSpace(n)
			}
			num := ConvertStringArrayToIntArray(splitNum)

			board = append(board, num)
		}
		b := createBoard(board)

		boards = append(boards, b)
	}

	return bingoNumbers, boards
}

func readFile(fileName string) []string {
	content, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Printf(err.Error())
	}

	lines := strings.Split(string(content), "\n\n")

	return lines
}

func calculateBoards(bingoNumbers []int, boards []Board) {
	bestBoard := Board{
		WinningNumberIndex: 10000,
	}

	worstBoard := Board{
		WinningNumberIndex: 0,
	}

	for _, board := range boards {
		board = board.calculateBoard(bingoNumbers)

		if board.WinningNumberIndex < bestBoard.WinningNumberIndex {
			bestBoard = board
		}

		if board.WinningNumberIndex > worstBoard.WinningNumberIndex {
			worstBoard = board
		}
	}

	PrintOutput(1, bestBoard.calculateSumOfUnmarked()*bestBoard.WinningNumber)
	PrintOutput(2, worstBoard.calculateSumOfUnmarked()*worstBoard.WinningNumber)
}

func (b Board) calculateBoard(bingoNumbers []int) Board {
	vWinningNumber, vNthNumber := b.calculateVerticalWin(bingoNumbers)
	hWinningNumber, hNthNumber := b.calculateHorizontalWin(bingoNumbers)

	if vNthNumber < hNthNumber {
		b.WinningNumber = vWinningNumber
		b.WinningNumberIndex = vNthNumber
	} else {
		b.WinningNumber = hWinningNumber
		b.WinningNumberIndex = hNthNumber
	}

	return b
}

func (b Board) calculateHorizontalWin(bingoNumbers []int) (int, int) {
	for index, number := range bingoNumbers {
		for _, line := range b.Lines {
			bo, ok := line[number]

			if bo.value == false && ok == true {
				line[number] = DictValue{bo.index, true}
			}

			trueCount := 0
			for _, v := range line {
				if v.value == true {
					trueCount++
				}
			}

			if trueCount == 5 {
				return number, index
			}
		}
	}

	return 0, 0
}

func (b Board) calculateVerticalWin(bingoNumbers []int) (int, int) {
	var newBoard [][]int

	currentVerticalLine := 0

	for i := 0; i < len(b.Lines[0]); i++ {
		var temp []int

		for _, line := range b.Lines {
			for k, v := range line {
				if v.index == currentVerticalLine {
					temp = append(temp, k)
				}
			}
		}

		newBoard = append(newBoard, temp)
		currentVerticalLine++
	}

	fmt.Println(newBoard)

	nb := createBoard(newBoard)

	return nb.calculateHorizontalWin(bingoNumbers)
}

func createBoard(board [][]int) Board {
	var dictBoard []map[int]DictValue
	for _, line := range board {
		dict := map[int]DictValue{
			line[0]: {index: 0, value: false},
			line[1]: {index: 1, value: false},
			line[2]: {index: 2, value: false},
			line[3]: {index: 3, value: false},
			line[4]: {index: 4, value: false},
		}

		dictBoard = append(dictBoard, dict)
	}

	return Board{Lines: dictBoard}
}

func (b Board) calculateSumOfUnmarked() int {
	left := 0
	for _, line := range b.Lines {
		for k, v := range line {
			if v.value == false {
				left += k
			}
		}
	}
	return left
}
