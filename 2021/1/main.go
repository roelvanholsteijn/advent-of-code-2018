package main

import (
	. "../../helpers"
)

func main() {
	numbers := ImportIntArrayFromFile("./2021/1/input.txt")

	largerThenPrevious(numbers)
	largerThenPreviousSlidingWindow(numbers)
}

func largerThenPrevious(numbers []int) {
	larger := 0

	for index, i := range numbers {
		if index == 0 {
			continue
		}

		if numbers[index-1] < i {
			larger++
		}
	}

	PrintOutput(1, larger)
}

func largerThenPreviousSlidingWindow(numbers []int) {
	larger := 0

	for index, _ := range numbers {
		if index < 3 {
			continue
		}

		if numbers[index-1]+numbers[index-2]+numbers[index-3] < numbers[index]+numbers[index-1]+numbers[index-2] {
			larger++
		}
	}

	PrintOutput(2, larger)
}
