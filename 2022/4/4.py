def part1(input):
    pairs = []

    for line in input:
        temp = line.split(',')
        first = temp[0].split('-')
        second = temp[1].split('-')

        first[0] = int(first[0])
        first[1] = int(first[1])

        second[0] = int(second[0])
        second[1] = int(second[1])

        pairs.append([first, second])

    overlapping = []
    for pair in pairs:
        if (pair[0][0] <= pair[1][0] and pair[0][1] >= pair[1][1]) or (
                pair[0][0] >= pair[1][0] and pair[0][1] <= pair[1][1]):
            overlapping.append(pair)

    print(len(overlapping))


def part2(input):
    pairs = []

    for line in input:
        temp = line.split(',')
        first = temp[0].split('-')
        second = temp[1].split('-')

        first[0] = int(first[0])
        first[1] = int(first[1])

        second[0] = int(second[0])
        second[1] = int(second[1])

        pairs.append([first, second])

    overlapping = []

    for pair in pairs:
        if pair[0][1] < pair[1][0] or pair[0][0] > pair[1][1]:
            continue
        overlapping.append(pair)

    print(len(overlapping))


if __name__ == "__main__":
    inp = open('input.txt').read().split('\n')

    part1(inp)
    part2(inp)
