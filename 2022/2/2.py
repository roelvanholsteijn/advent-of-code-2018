if __name__ == "__main__":
    lines = open('input.txt').read().split('\n')

    scoreOptionsPart1 = {
        "A X": 4,
        "A Y": 8,
        "A Z": 3,
        "B X": 1,
        "B Y": 5,
        "B Z": 9,
        "C X": 7,
        "C Y": 2,
        "C Z": 6
    }

    scoreOptionsPart2 = {
        "A X": 3,
        "A Y": 4,
        "A Z": 8,
        "B X": 1,
        "B Y": 5,
        "B Z": 9,
        "C X": 2,
        "C Y": 6,
        "C Z": 7
    }

    sumPart1 = 0
    sumPart2 = 0
    for line in lines:
        sumPart1 += scoreOptionsPart1[line]
        sumPart2 += scoreOptionsPart2[line]

    print(sumPart1)
    print(sumPart2)
