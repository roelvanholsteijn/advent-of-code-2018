def find_first_marker_of_size(string, size):
    for i in range(len(string)):
        if len(''.join(set(string[i: i + size]))) == size:
            return i + size


if __name__ == "__main__":
    inp = open('input.txt').read()

    print(find_first_marker_of_size(inp, 4))
    print(find_first_marker_of_size(inp, 14))
