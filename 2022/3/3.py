def part1(lines):
    faults = []

    for line in lines:
        line_len = len(line)

        first_compartment = line[0:int(line_len / 2)]
        second_compartment = line[int(line_len / 2):line_len]

        for char in first_compartment:
            if second_compartment.count(char) == 0:
                continue

            faults.append(char)
            break

    faultsCount = []
    for char in faults:
        if char.isupper():
            faultsCount.append(ord(char) - 38)
        else:
            faultsCount.append(ord(char) - 96)

    print(sum(faultsCount))


def part2(lines):
    groups = []
    group = []

    for index, line in enumerate(lines):
        group.append(line)
        if (index + 1) % 3 == 0:
            groups.append(group)
            group = []

    badges = []

    for group in groups:
        for char in group[0]:
            if (group[1].count(char) > 0) & (group[2].count(char) > 0):
                badges.append(char)
                break

    numbers = get_correct_priority_numbers(badges)

    print(sum(numbers))


def get_correct_priority_numbers(chars):
    correct_numbers = []
    for char in chars:
        if char.isupper():
            correct_numbers.append(ord(char) - 38)
        else:
            correct_numbers.append(ord(char) - 96)

    return correct_numbers


if __name__ == "__main__":
    inp = open('input.txt').read().split('\n')

    part1(inp)

    part2(inp)
