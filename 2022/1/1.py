def get_input():
    f = open("input.txt", "r")

    lines = []

    for line in f:
        lines.append(line.replace("\n", ""))

    return lines


if __name__ == "__main__":
    lines = get_input()

    elves = []
    cals = 0

    for line in lines:
        if len(line) == 0:
            elves.append(cals)
            cals = 0
            continue
        cals += int(line)

    elves.sort()
    elves.reverse()

    # part 1
    print(elves[0])

    # part 2
    print(elves[0]+elves[1]+elves[2])
