class Folder:
    name = ''
    size = 0
    files = []
    folders = []

    def __init__(self, name):
        self.name = name


if __name__ == "__main__":
    inp = open('test_input.txt').read().split('\n')

    current_folder = ['/']

    folder_dict = {
        '/': []
    }

    for line in inp:
        if line[0:1] == '$':
            if line[2:4] == 'cd':
                cd = line.split('cd ')

                if cd[1] == '/':
                    current_folder = ['/']
                elif cd[1] == '..':
                    current_folder.pop(0)
                else:
                    current_folder.insert(0, cd[1])
                    folder_dict[''.join(current_folder)] = []

        else:
            folder_dict[''.join(current_folder)].append(line)