def create_stack(puzzle_input):
    stack_list = puzzle_input[0].split('\n')

    stack_list.reverse()

    stack_dict = {}

    for index, value in enumerate(stack_list[0]):
        if not value.isspace():
            column = []
            for container in range(1, len(stack_list)):
                c = stack_list[container][index]

                if not c.isspace():
                    column.insert(0, c)

            stack_dict[value] = column

    return stack_dict


def create_moves(puzzle_input):
    move_instructions = puzzle_input[1].split('\n')

    moves = []
    for i in move_instructions:
        instruction = [int(s) for s in i.split() if s.isdigit()]
        moves.append(instruction)

    return moves


def create_message_from_dict(input_dict):
    message = ''

    for number in range(len(input_dict)):
        message += input_dict[str(number + 1)][0]

    return message


def part1(puzzle_input):
    stack_dict = create_stack(puzzle_input)
    moves = create_moves(puzzle_input)

    for move in moves:
        for i in range(move[0]):
            stack_dict[str(move[2])].insert(0, stack_dict[str(move[1])][0])
            stack_dict[str(move[1])].pop(0)

    print(create_message_from_dict(stack_dict))


def part2(puzzle_input):
    stack_dict = create_stack(puzzle_input)
    moves = create_moves(puzzle_input)

    for move in moves:
        temp = stack_dict[str(move[1])][0: move[0]]

        stack_dict[str(move[2])][0:0] = temp
        stack_dict[str(move[1])] = stack_dict[str(move[1])][move[0]:]

    print(create_message_from_dict(stack_dict))


if __name__ == "__main__":
    inp = open('input.txt').read().split('\n\n')

    part1(inp)
    part2(inp)
