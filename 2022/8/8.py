def is_visible(grid, x, y):
    tree = grid[x][y]

    print('-----')
    print('-----')
    print(tree)

    for i in range(0, len(grid[0])):
        if i == x:
            continue
        if tree > grid[i][y]:
            print(grid[i][y])
            return True

    for i in range(0, len(grid)):
        if i == y:
            continue
        if tree > grid[x][i]:
            print(grid[x][i])
            return True

    return False


if __name__ == "__main__":
    inp = open('test_input.txt').read().split('\n')

    grid = [[*i] for i in inp]

    counter = 0
    for i in range(1, len(grid) - 1):
        for j in range(1, len(grid[0]) - 1):
            if is_visible(grid, i, j):
                counter += 1

    counter += (len(grid[0])*2) + (len(grid)*2) - 4

    print(counter)